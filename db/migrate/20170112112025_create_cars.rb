class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :name
      t.string :plate
      t.integer :year
      t.float :value

      t.timestamps null: false
    end
  end
end
