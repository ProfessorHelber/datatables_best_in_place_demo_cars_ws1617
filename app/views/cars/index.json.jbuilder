json.array!(@cars) do |car|
  json.extract! car, :id, :name, :plate, :year, :value
  json.url car_url(car, format: :json)
end
